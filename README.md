//sudo node simple-device.js "90b6860f8d92"

rfc-ble-device
==============

RFCode's Node.js lib to abstract BLE (Bluetooth Low Energy) central module per the [RFCode's BLE GATT specification](https://github.com/rfcode/Specifications/blob/master/Bluetooth%20IR%20Rack%20Locator%20GATT%20Specification.pdf), using [noble](https://github.com/sandeepmistry/noble)

## Install
```
npm install rfc-ble-device
```

## Prerequisites

### OS X

 * install [Xcode](https://itunes.apple.com/ca/app/xcode/id497799835?mt=12)

### Linux

 * Kernel version 3.6 or above
 * ```libbluetooth-dev```

#### Ubuntu/Debian/Raspbian

```sh
sudo apt-get install bluetooth bluez libbluetooth-dev libudev-dev
```

Make sure ```node``` is on your path, if it's not, some options:
 * symlink ```nodejs``` to ```node```: ```sudo ln -s /usr/bin/nodejs /usr/bin/node```
 * [install Node.js using the NodeSource package](https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions)

#### Fedora / Other-RPM based

```sh
sudo yum install bluez bluez-libs bluez-libs-devel
```

## Usage

Description: TODO
See examples folder to see how to create a new device based on this module rfc-ble-device.

```javascript
var RfcBleDevice = require('rfc-ble-device');

### Discovery API

#### One

```javascript
RfcBleDevice.discover(callback(peripheralDevice));
```

#### Discover All

```javascript
function onDiscover(peripheralDevice) {
  // ...
}

RfcBleDevice.discoverAll(onDiscover);

RfcBleDevice.stopDiscoverAll(onDiscover);
```

#### By id

```javascript
RfcBleDevice.discoverById(id, callback(peripheralDevice));
```

#### By address

```javascript
RfcBleDevice.discoverByAddress(address, callback(peripheralDevice));
```
