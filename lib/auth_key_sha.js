/*
The Auth Key must be written correctly by the client in order to provide authentication 
and allow other characteristics to be written. The Auth Key is the SHA256 hash generated 
from a specific combination of the Auth Token and the device password. 
*/
var crypto = require('crypto');

var BUFSIZE = 16;
var PASS_MIN = 4;
var PASS_MAX = 14;

/*
The method for combining the Auth Token and device password is to start with a 16-byte buffer 
and fill it with multiple copies of the Auth Token in little-endian format 
(with the LSB at buffer[0, 4, 8, and 12]). 

For example, if the Auth Token was 0x1234ABCD the buffer would be: 
CD AB 34 12 CD AB 34 12 CD AB 34 12 CD AB 34 12  
  
You then XOR the device password into the first ‘n’ bytes of the buffer (where ‘n’ is the current password length). 
With the default password of ‘RFCode’ this would be: 
CD^52 AB^46 34^43 12^6F CD^64 AB^65 34 12 CD AB 34 12 CD AB 34 12  

which results in these buffer contents: 
9F ED 77 7D A9 CE 34 12 CD AB 34 12 CD AB 34 12  

The 16-byte buffer is then hashed via SHA256 starting with buffer[0] and ending with buffer[15]. 
Using the example buffer data, the resulting SHA256 hash is: 

8E7BB8C971669F2FAB84CF6670AFAE82A8A1C851220B297EDAA6F00A7CC0B16E 

*/

var deriveAuthKeySha256 = function(token, password) {
	var i;

	if(typeof token !== 'number')
		throw new TypeError('Token should be an integer');
	if(typeof password !== 'string')
		throw new TypeError('Password should be a string');

	if(token < 0 || token > 0xFFFFFFFF)
		throw new Error('Illegal token value');

	var passlen = password.length;
	if(passlen < PASS_MIN)
		throw new Error('Password is too short');
	else if(passlen > PASS_MAX)
		throw new Error('Password is too long');

	/*
	4.4.5 Auth Token (uint32_t, R) uuid=0x6a730000-527f-11e5-8682-0002d5a5c51b -is a 32-bit token value 
	that is randomly generated each time a BLE connection is established 
	
	split this Auth Token into bytes, in little endian (LSB goes first)
	*/
	var tokenbytes = [];
	for(i=0; i<4; i++) {
		tokenbytes.push(token & 0xFF);
		token = token >> 8;
	}

	/*
	start with a 16-byte buffer and fill it up with multiple copies of auth token from the previous step
	*/
	
	var buf = new Buffer(BUFSIZE); // 16 uninitialized bytes
	// fill with token bytes
	for(i=0; i<BUFSIZE; i++) {
		buf[i] = tokenbytes[i % 4];
	}

	/*
	Now XOR the device password into the first 'n' bytes of the buffer (where 'n' is the user entered/default password length)
	*/
	// now let's XOR buffer with password
	for(i=0, len=password.length; i<len; i++) {
		buf[i] ^= password.charCodeAt(i);
	}

	/*
	The 16-byte buffer is then hashed via SHA256

	Note: if you are to replace a different library for generating sha 256, please ensure that it does work with our device firmware.
	Device firmware is implemented in C, you might run into issues due to the data type precision discrepancies between javascript and C.

	For sake of understanding/documentational purposes I have pasted the firmware code that generates the HASH and compares it to the one 
	provided by the BLE client to determine it is valid (true) or not (false).

		static bool validate_auth_key(const uint8_t *data)
		{
	        SHA256_CTX shactx;
	        uint8_t digest[32] __attribute__((aligned(4)));
	        int i;
	        bool result = true;

	        // Initialize the SHA context
	        SHA256_Init(&shactx);
	       
	        // Use digest[0-15] to buffer the data we want to hash
	        *(uint32_t *)&digest[0] = auth_token;
	        *(uint32_t *)&digest[4] = auth_token;
	        *(uint32_t *)&digest[8] = auth_token;
	        *(uint32_t *)&digest[12] = auth_token;
	        // XOR the device password into the buffer
	        for (i=0; i<MAX_PWD_LEN; i++)
	        {
	            if (!config.password[i]) break;
	            digest[i] ^= config.password[i];
	        }

	        // Pass in the 16-bytes of buffer to be hashed
	        SHA256_Update(&shactx, digest, 16);
	       
	        // Finalize the hash and obtain the SHA256 digest
	        SHA256_Final(digest, &shactx);

	        // Compare each byte in the hash to the key that was written
	        for (i=0;i<sizeof(digest);i++)
	        {
	            if (digest[i] != data[i])
	            {
	                result = false;
	                break;
	            }
	        }
	        return result;
		}
	*/

	// calculate SHA256 of the buffer
	var hash = new crypto.Hash('sha256');
	hash.update(buf);
	
	/*
		Calculates the digest of all of the data passed to be hashed (using the hash.update() method). 
		The encoding can be 'hex', 'latin1' or 'base64'. If encoding is provided a string will be returned; otherwise a Buffer is returned.
	*/	
	// return hash.digest('hex');
	return hash.digest();
};

module.exports = deriveAuthKeySha256;
