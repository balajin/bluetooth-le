/*
RFCode BLE Devices

This modules uses noble library for node.js to create a central server that reads and connects to BLE peripherals 
and sends this info to the client via socket.io

Note: A740/A741 are peripheral devices and the client is called Central, which will be implemented by this module.

created on 02 May 2016
by RFCode, Inc.

-------
When you connect as a central to a device, you can read data from it. 
This is what the data structure of a BLE device looks like this:

-device (peripheral)
--  services
---    characteristics
----      descriptors

Services group functionality.
Characteristics represent data that you can read, write, or subscribe to using notifications.
Descriptors contain “meta-data” for characteristics, however not all characteristics have descriptors.

To connect to a BLE device, you first scan for BLE devices, then connect to the device you wish to use. 
Here is an outline:

Scan for devices.
When a device is found, you can identify the device, and determine its RSSI value.
When the device you want is found, you can connect to it.
When connected you can read services, characteristics, descriptors.
Then write and/or read the characteristics you wish to use. Enable any notifications you want to use.
-------

*/

var events = require('events');
var util = require('util');

// Generic Access Refer to section 4.1 of 
var GENERIC_ACCESS_UUID                     = '1800';
var DEVICE_NAME_UUID                        = '2a00';

function LocatorDevice(peripheral) {
  this._peripheral = peripheral;
  this._services = {};
  this._characteristics = {};

  this.id = peripheral.id;
  this.uuid = peripheral.uuid; // for legacy
  this.address = peripheral.address;
  this.addressType = peripheral.addressType;
  this.connectedAndSetUp = false;

  // this._peripheral.on('disconnect', this.onDisconnect.bind(this));
}

util.inherits(LocatorDevice, events.EventEmitter);

LocatorDevice.prototype.onDisconnect = function() {
  this.connectedAndSetUp = false;
  this.emit('disconnect');
};

LocatorDevice.prototype.toString = function() {
  return JSON.stringify({
    uuid: this.uuid
  });
};

LocatorDevice.prototype.connect = function(callback) {
  // this._peripheral.connect(callback);
  this._peripheral.connect(function(error) {
      if (!error) {
        this._peripheral.once('disconnect', this.onDisconnect.bind(this));
      }
      if (typeof(callback) === 'function') {
        callback(error);
      }
    }.bind(this));
};

// when a peripheral disconnects
LocatorDevice.prototype.disconnect = function(callback) {
  this._peripheral.disconnect(callback);
};

LocatorDevice.prototype.discoverServicesAndCharacteristics = function(callback) {
  this._peripheral.discoverAllServicesAndCharacteristics(function(error, services/*, characteristics*/) {
    if (error) {
      return callback(error);
    }

    for (var i in services) {
      var service = services[i];
      var characteristics = service.characteristics;

      var serviceUuid = service.uuid;

      this._services[serviceUuid] = service;
      this._characteristics[serviceUuid] = {};

      for (var j in characteristics) {
        var characteristic = characteristics[j];

        this._characteristics[serviceUuid][characteristic.uuid] = characteristic;
      }
    }

    callback();
  }.bind(this));
};

// the connect function. This is local to the discovery function
// because it needs the peripheral to discover services
LocatorDevice.prototype.connectAndSetUp = LocatorDevice.prototype.connectAndSetup = function(callback) {
  this.connect(function(error) {
    if (error) {
      return callback(error);
    }

    this.discoverServicesAndCharacteristics(function() {
      this.connectedAndSetUp = true;
      callback();
    }.bind(this));
  }.bind(this));
};

LocatorDevice.prototype.hasService = function(serviceUuid) {
  return (!!this._characteristics[serviceUuid]);
};

LocatorDevice.prototype.hasCharacteristic = function(serviceUuid, characteristicUuid) {
  return this.hasService(serviceUuid) && (!!this._characteristics[serviceUuid][characteristicUuid]);
};

LocatorDevice.prototype.readDataCharacteristic = function(serviceUuid, characteristicUuid, callback) {
  if (!this.hasService(serviceUuid)) {
    return callback(new Error('service uuid ' + serviceUuid + ' not found!'));
  } else if (!this.hasCharacteristic(serviceUuid, characteristicUuid)) {
    return callback(new Error('characteristic uuid ' + characteristicUuid + ' not found in service uuid ' + serviceUuid + '!'));
  }

  this._characteristics[serviceUuid][characteristicUuid].read(callback);
};

LocatorDevice.prototype.writeDataCharacteristic = function(serviceUuid, characteristicUuid, data, callback) {
  if (!this.hasService(serviceUuid)) {
    return callback(new Error('service uuid ' + serviceUuid + ' not found!'));
  } else if (!this.hasCharacteristic(serviceUuid, characteristicUuid)) {
    return callback(new Error('characteristic uuid ' + characteristicUuid + ' not found in service uuid ' + serviceUuid + '!'));
  }

  var characteristic = this._characteristics[serviceUuid][characteristicUuid];

  var withoutResponse = (characteristic.properties.indexOf('writeWithoutResponse') !== -1) &&
                          (characteristic.properties.indexOf('write') === -1);

  characteristic.write(data, withoutResponse, function(error) {
    // error might be undefined if everything went good
    if (typeof callback === 'function') {
      callback(error);
    }
  });
};
LocatorDevice.prototype.writeDataCharacteristicWithResponse = function(serviceUuid, characteristicUuid, data, callback) {
  if (!this.hasService(serviceUuid)) {
    return callback(new Error('service uuid ' + serviceUuid + ' not found!'));
  } else if (!this.hasCharacteristic(serviceUuid, characteristicUuid)) {
    return callback(new Error('characteristic uuid ' + characteristicUuid + ' not found in service uuid ' + serviceUuid + '!'));
  }

  var characteristic = this._characteristics[serviceUuid][characteristicUuid];

  var withoutResponse = true;

  characteristic.write(data, withoutResponse, function(error) {
    // error might be undefined if everything went good
    if (typeof callback === 'function') {
      callback(error);
    }
  });
};
LocatorDevice.prototype.notifyCharacteristic = function(serviceUuid, characteristicUuid, notify, listener, callback) {
  if (!this.hasService(serviceUuid)) {
    return callback(new Error('service uuid ' + serviceUuid + ' not found!'));
  } else if (!this.hasCharacteristic(serviceUuid, characteristicUuid)) {
    return callback(new Error('characteristic uuid ' + characteristicUuid + ' not found in service uuid ' + serviceUuid + '!'));
  }

  var characteristic = this._characteristics[serviceUuid][characteristicUuid];

  characteristic.notify(notify, function(error) {
    if (notify) {
      characteristic.addListener('data', listener);
    } else {
      characteristic.removeListener('data', listener);
    }

    if (typeof callback === 'function') {
      callback(error);
    }
  });
};

LocatorDevice.prototype.subscribeCharacteristic = function(serviceUuid, characteristicUuid, listener, callback) {
  this.notifyCharacteristic(serviceUuid, characteristicUuid, true, listener, callback);
};

LocatorDevice.prototype.unsubscribeCharacteristic = function(serviceUuid, characteristicUuid, listener, callback) {
  this.notifyCharacteristic(serviceUuid, characteristicUuid, false, listener, callback);
};

LocatorDevice.prototype.readStringCharacteristic = function(serviceUuid, characteristicUuid, callback) {
  this.readDataCharacteristic(serviceUuid, characteristicUuid, function(error, data) {
    if (error) {
      return callback(error);
    }

    callback(null, data.toString());
  });
};

LocatorDevice.prototype.readUInt8Characteristic = function(serviceUuid, characteristicUuid, callback) {
  this.readDataCharacteristic(serviceUuid, characteristicUuid, function(error, data) {
    if (error) {
      return callback(error);
    }

    callback(null, data.readUInt8(0));
  });
};

LocatorDevice.prototype.readUInt8ArrayCharacteristic = function(serviceUuid, characteristicUuid, callback) {
  this.readDataCharacteristic(serviceUuid, characteristicUuid, function(error, data) {
    if (error) {
      return callback(error);
    }
    //will return an array of values.  Please refer to GATT specs. to figure of which array elemens maps to what.
    /* For Ex: data for 4.4.14(Segment Status) could be [5, 0, 91, 0, 5, 0, 95, 0]
    For Ex: 4.4.14 Segment Status
    uint8_t;  // Detected Segments in Bank 1
    uint8_t;  // Reserved
    uint16_t; // LED current in Bank 1 
    uint8_t;  // Detected Segments in Bank 2
    uint8_t;  // Reserved
    uint16_t; // LED current in Bank 2

    In which case, Bank 1 Detected segment is 5(array 0) and Bank 2 Detected segment is 5 (array 4).
*/
    callback(null, new Uint16Array(data)); //Note: data is buffer(8) 

  });
};
LocatorDevice.prototype.readInt8Characteristic = function(serviceUuid, characteristicUuid, callback) {
  this.readDataCharacteristic(serviceUuid, characteristicUuid, function(error, data) {
    if (error) {
      return callback(error);
    }

    callback(null, data.readInt8(0));
  });
};

LocatorDevice.prototype.readUInt16LECharacteristic = function(serviceUuid, characteristicUuid, callback) {
  this.readDataCharacteristic(serviceUuid, characteristicUuid, function(error, data) {
    if (error) {
      return callback(error);
    }

    callback(null, data.readUInt16LE(0));
  });
};

LocatorDevice.prototype.readInt16LECharacteristic = function(serviceUuid, characteristicUuid, callback) {
  this.readDataCharacteristic(serviceUuid, characteristicUuid, function(error, data) {
    if (error) {
      return callback(error);
    }

    callback(null, data.readInt16LE(0));
  });
};

LocatorDevice.prototype.readUInt32BECharacteristic = function(serviceUuid, characteristicUuid, callback) {
  this.readDataCharacteristic(serviceUuid, characteristicUuid, function(error, data) {
    if (error) {
      return callback(error);
    }

    callback(null, data.readUInt32BE(0));
  });
};

LocatorDevice.prototype.readUInt32LECharacteristic = function(serviceUuid, characteristicUuid, callback) {
  this.readDataCharacteristic(serviceUuid, characteristicUuid, function(error, data) {
    if (error) {
      return callback(error);
    }

    callback(null, data.readUInt32LE(0));
  });
};

LocatorDevice.prototype.readInt32LECharacteristic = function(serviceUuid, characteristicUuid, callback) {
  this.readDataCharacteristic(serviceUuid, characteristicUuid, function(error, data) {
    if (error) {
      return callback(error);
    }

    callback(null, data.readInt32LE(0));
  });
};

LocatorDevice.prototype.readFloatLECharacteristic = function(serviceUuid, characteristicUuid, callback) {
  this.readDataCharacteristic(serviceUuid, characteristicUuid, function(error, data) {
    if (error) {
      return callback(error);
    }

    callback(null, data.readFloatLE(0));
  });
};

LocatorDevice.prototype.writeStringCharacteristic = function(serviceUuid, characteristicUuid, string, callback) {
  this.writeDataCharacteristic(serviceUuid, characteristicUuid, new Buffer(string), callback);
};
LocatorDevice.prototype.writeStringCharacteristic2 = function(serviceUuid, characteristicUuid, string, callback) {
  this.writeDataCharacteristicWithResponse(serviceUuid, characteristicUuid, new Buffer(string), callback);
};

LocatorDevice.prototype.writeUInt8Characteristic = function(serviceUuid, characteristicUuid, value, callback) {
  var buffer = new Buffer(1);
  buffer.writeUInt8(value, 0);

  this.writeDataCharacteristic(serviceUuid, characteristicUuid, buffer, callback);
};

LocatorDevice.prototype.writeInt8Characteristic = function(serviceUuid, characteristicUuid, value, callback) {
  var buffer = new Buffer(1);
  buffer.writeInt8(value, 0);

  this.writeDataCharacteristic(serviceUuid, characteristicUuid, buffer, callback);
};
LocatorDevice.prototype.writeUInt8Array2Characteristic = function(serviceUuid, characteristicUuid, value, callback) {
  var buffer = new Buffer.from(value);
  // buffer.writeUInt8(value,0,value.length);
  // buffer.writeUIntBE(value,0,2);
  this.writeDataCharacteristic(serviceUuid, characteristicUuid, buffer, callback);
};
LocatorDevice.prototype.writeUInt8Array32Characteristic = function(serviceUuid, characteristicUuid, value, callback) {
  // var buffer = new Buffer.from(value);
  // buffer.writeUInt8(value,0,value.length);
  // buffer.writeUIntBE(value,0,2);
  // console.log('buffer:', buffer);
  this.writeDataCharacteristic(serviceUuid, characteristicUuid, value, callback);
};

LocatorDevice.prototype.writeUInt16BECharacteristic = function(serviceUuid, characteristicUuid, value, callback) {
  var buffer = new Buffer(2);
  buffer.writeUInt16BE(value, 0);

  this.writeDataCharacteristic(serviceUuid, characteristicUuid, buffer, callback);
};

LocatorDevice.prototype.writeUInt16LECharacteristic = function(serviceUuid, characteristicUuid, value, callback) {
  var buffer = new Buffer(2);
  buffer.writeUInt16LE(value, 0);

  this.writeDataCharacteristic(serviceUuid, characteristicUuid, buffer, callback);
};

LocatorDevice.prototype.writeInt16LECharacteristic = function(serviceUuid, characteristicUuid, value, callback) {
  var buffer = new Buffer(2);
  buffer.writeInt16LE(value, 0);

  this.writeDataCharacteristic(serviceUuid, characteristicUuid, buffer, callback);
};

LocatorDevice.prototype.writeUInt32BECharacteristic = function(serviceUuid, characteristicUuid, value, callback) {
  var buffer = new Buffer(4);
  buffer.writeUInt32BE(value, 0);

  this.writeDataCharacteristic(serviceUuid, characteristicUuid, buffer, callback);
};

LocatorDevice.prototype.writeUInt32LECharacteristic = function(serviceUuid, characteristicUuid, value, callback) {
  var buffer = new Buffer(4);
  buffer.writeUInt32LE(value, 0);

  this.writeDataCharacteristic(serviceUuid, characteristicUuid, buffer, callback);
};

LocatorDevice.prototype.writeInt32LECharacteristic = function(serviceUuid, characteristicUuid, value, callback) {
  var buffer = new Buffer(4);
  buffer.writeInt32LE(value, 0);

  this.writeDataCharacteristic(serviceUuid, characteristicUuid, buffer, callback);
};

LocatorDevice.prototype.writeFloatLECharacteristic = function(serviceUuid, characteristicUuid, value, callback) {
  var buffer = new Buffer(4);
  buffer.writeFloatLE(value, 0);

  this.writeDataCharacteristic(serviceUuid, characteristicUuid, buffer, callback);
};

LocatorDevice.prototype.readDeviceName = function(callback) {
  this.readStringCharacteristic(GENERIC_ACCESS_UUID, DEVICE_NAME_UUID, callback);
};

module.exports = LocatorDevice;
