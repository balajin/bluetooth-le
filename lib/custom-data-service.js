/* 
Custom Data Service
This custom service provides access to the configuration and operation of the 
Bluetooth IR Rack Locator. 

Writing to any characteristic will fail until the Auth Key (4.4.5) has been written 
with the correct value.
*/

var CUSTOM_DATA_UUID      = '53124a80527f11e5af390002a5d5c51b';

var DEVICE_NAME_UUID        = '2a00'; //4.4.1 Device Name (string, R/W) user-editable device name and can be up to 16 bytes in length. The default value for this field is the Device Serial Number
var RECONNECTION_ADDR_UUID  = '2a03'; //4.4.2 Reconnection Address (uint8_t[6], R/W)  write-once serial number that is 48 bits in length. It is written during manufacturing.
var SERIAL_NUMBER_UUID      = '2a25'; //4.4.2 Serial Number (string, R/W)  write-once serial number that is 14 bytes in length. It is written during manufacturing.
var ALERT_LEVEL_UUID        = '2a06'; //4.4.3 Alert Level (uint8_t, R/W) Writing this characteristic will start/stop blinking of a status LED and/or the visible LEDs in the LED strips. This function is intended to help identify the rack in which this Bluetooth IR Rack Locator is installed. The value of the characteristic always returns to 0 when the BLE connection is terminated.
var AUTH_TOKEN_UUID         = '6a730000527f11e586820002a5d5c51b';  //4.4.4 Auth Token (uint32_t, R)
var AUTH_KEY_UUID           = '6a730001527f11e586820002a5d5c51b';  //4.4.5 Auth Key (uint8_t[32], W)
var CONFIG_HASH_UUID        = '6a730002527f11e586820002a5d5c51b';  //4.4.6 Configuration Hash (uint16_t, R)
var PASSWORD_UUID           = '6a730003527f11e586820002a5d5c51b';  //4.4.7 Password (string, W)
var ERROR_STATUS_UUID       = '6a730010527f11e586820002a5d5c51b';  //4.4.8 Error Status (uint8_t, R/N)
var LOCATION_CODE_UUID      = '6a730011527f11e586820002a5d5c51b';  //4.4.9 IR Location Code (uint16_t, R/W)
var BRIGHTNESS_UUID         = '6a730012527f11e586820002a5d5c51b';  //4.4.10 Brightness (uint8_t, R/W)
var BANK_ENABLE_UUID        = '6a730013527f11e586820002a5d5c51b';  //4.4.11 Bank Enable (uint16_t, R/W)
var EXPECTED_SEGMENTS_UUID  = '6a730014527f11e586820002a5d5c51b';  //4.4.12 Expected Segments (uint8_t[2], R/W)
var SEGMENT_STATUS_UUID     = '6a730015527f11e586820002a5d5c51b';  //4.4.13 Segment Status (R/N)
var CALIBRATION_DATA_UUID   = '6a730016527f11e586820002a5d5c51b';  //4.4.14 Calibration Data (uint16_t[2][10], R)
var START_CALIBRATION_UUID  = '6a730020527f11e586820002a5d5c51b';  //4.4.15 Start Calibration (uint8_t, R/W/N)
var ADVERTISING_CONFIG_UUID = '6a730040527f11e586820002a5d5c51b';  //4.4.16 Advertising Config (R/W)


function CustomDataService() {
}

CustomDataService.prototype.readDeviceName = function(callback) {
  this.readStringCharacteristic(CUSTOM_DATA_UUID, DEVICE_NAME_UUID, callback);
};

CustomDataService.prototype.readReconnectionAddr = function(callback) {
  this.readDataCharacteristic(CUSTOM_DATA_UUID, RECONNECTION_ADDR_UUID, callback);
};
CustomDataService.prototype.writeReconnectionAddr = function(value,callback) {
  this.writeStringCharacteristic(CUSTOM_DATA_UUID, RECONNECTION_ADDR_UUID, value, callback);
};
CustomDataService.prototype.readSerialNumber = function(callback) {
  this.readStringCharacteristic(CUSTOM_DATA_UUID, SERIAL_NUMBER_UUID, callback);
};
CustomDataService.prototype.writeSerialNumber = function(value,callback) {
  //TODO: for testing -writeStringCharacteristic2 is a clone of writeStringCharacteristic to set withoutResponse to true
  this.writeStringCharacteristic2(CUSTOM_DATA_UUID, SERIAL_NUMBER_UUID, value, callback);
};

CustomDataService.prototype.readAlertLevel = function(callback) {
  this.readUInt8Characteristic(CUSTOM_DATA_UUID, ALERT_LEVEL_UUID, callback);
};
CustomDataService.prototype.writeAlertLevel = function(value, callback) {
  this.writeUInt8Characteristic(CUSTOM_DATA_UUID, ALERT_LEVEL_UUID, value, callback);
};

CustomDataService.prototype.readAuthToken = function(callback) {
  this.readUInt32LECharacteristic(CUSTOM_DATA_UUID, AUTH_TOKEN_UUID, callback);
};

CustomDataService.prototype.writeAuthKey = function(value, callback) {
  this.writeUInt8Array32Characteristic(CUSTOM_DATA_UUID, AUTH_KEY_UUID, value, callback);
};
CustomDataService.prototype.readConfigHash = function(callback) {
  this.readUInt16LECharacteristic(CUSTOM_DATA_UUID, CONFIG_HASH_UUID, callback);
};

CustomDataService.prototype.writePassword = function(value, callback) {
  this.writeStringCharacteristic(CUSTOM_DATA_UUID, PASSWORD_UUID, value, callback);
};

/*
4.4.8 Error Status (uint8_t, R/N)
This is the error status of the Bluetooth IR Rack Locator. This is a bitmask field with: bit 0 (0x01) = Power Error
bit 1 (0x02) = Bank Unplugged bit 2 (0x04) = Bank Overload
bit 3 (0x08) = Bank Segment Lost bit 4 (0x10) = Reserved
bit 5 (0x20) = Reserved   bit 6 (0x40) = Reserved   bit 7 (0x80) = Reserved

The Error Status is also included in the Advertisement packet.
*/
CustomDataService.prototype.notifyErrorStatus = function(callback) {
  this.onErrorStatusChangeBinded = this.onMeasumentChange.bind(this);
  this.notifyCharacteristic(CUSTOM_DATA_UUID, ERROR_STATUS_UUID, true, this.onErrorStatusChangeBinded, callback);
};
CustomDataService.prototype.unnotifyErrorStatus = function(callback) {
  this.notifyCharacteristic(CUSTOM_DATA_UUID, ERROR_STATUS_UUID, false, this.onErrorStatusChangeBinded, callback);
};
CustomDataService.prototype.onErrorStatusChange = function(data) {
  this.convertMeasument(data, function(counter) {
    this.emit('errorStatusChange', counter);
  }.bind(this));
};
CustomDataService.prototype.readErrorStatus = function(callback) {
  this.readUInt8Characteristic(CUSTOM_DATA_UUID, ERROR_STATUS_UUID, callback);
};


CustomDataService.prototype.readLocationCode = function(callback) {
  this.readUInt16LECharacteristic(CUSTOM_DATA_UUID, LOCATION_CODE_UUID, callback);
};
CustomDataService.prototype.writeLocationCode = function(value, callback) {
  this.writeUInt16LECharacteristic(CUSTOM_DATA_UUID, LOCATION_CODE_UUID, value, callback);
};

CustomDataService.prototype.readBrightness = function(callback) {
  this.readUInt8Characteristic(CUSTOM_DATA_UUID, BRIGHTNESS_UUID, callback);
};
CustomDataService.prototype.writeBrightness = function(value, callback) {
  this.writeUInt8Characteristic(CUSTOM_DATA_UUID, BRIGHTNESS_UUID, value, callback);
};

CustomDataService.prototype.readBankEnable = function(callback) {
  this.readUInt16LECharacteristic(CUSTOM_DATA_UUID, BANK_ENABLE_UUID, callback);
};
CustomDataService.prototype.writeBankEnable = function(value, callback) {
  this.writeUInt16LECharacteristic(CUSTOM_DATA_UUID, BANK_ENABLE_UUID, value, callback);
};

CustomDataService.prototype.readExpectedSegments = function(callback) {
  // this.readUInt8Characteristic(CUSTOM_DATA_UUID, EXPECTED_SEGMENTS_UUID, callback);
  this.readUInt8ArrayCharacteristic(CUSTOM_DATA_UUID, EXPECTED_SEGMENTS_UUID, callback);
};
CustomDataService.prototype.writeExpectedSegments = function(value, callback) {
  // this.writeUInt8Characteristic(CUSTOM_DATA_UUID, EXPECTED_SEGMENTS_UUID, value, callback);
  this.writeUInt8Array2Characteristic(CUSTOM_DATA_UUID, EXPECTED_SEGMENTS_UUID, value, callback);
};

/* 4.4.13 Segment Status (R/N)
This is for diagnostic purposes.
This characteristic contains the IR LED segment status of the Bluetooth IR Rack Locator, indicating how 
many segments are detected and the peak LED current flowing in each bank (all values in little-endian format):

uint8_t;  // Detected Segments in Bank 1    Range: 0, 3-5, 255
uint8_t;  // Reserved
uint16_t; // LED current in Bank 1        Range: 0-65535 (mA) 
uint8_t;  // Detected Segments in Bank 2    Range: 0, 3-5, 255
uint8_t;  // Reserved
uint16_t; // LED current in Bank 2        Range: 0-65535 (mA)

If the bank is in over-current the reported segment count will be 255 (0xFF) and the reported current 
for that bank will be 65535 mA (0xFFFF).
*/
CustomDataService.prototype.notifySegmentStatus = function(callback) {
  this.onSegmentStatusChangeBinded = this.onMeasumentChange.bind(this);
  this.notifyCharacteristic(CUSTOM_DATA_UUID, SEGMENT_STATUS_UUID, true, this.onSegmentStatusChangeBinded, callback);
};
CustomDataService.prototype.unnotifySegmentStatus = function(callback) {
  this.notifyCharacteristic(CUSTOM_DATA_UUID, SEGMENT_STATUS_UUID, false, this.onSegmentStatusChangeBinded, callback);
};
CustomDataService.prototype.onSegmentStatusChange = function(data) {
  this.convertMeasument(data, function(counter) {
    this.emit('segmentStatusChange', counter);
  }.bind(this));
};
CustomDataService.prototype.readSegmentStatus = function(callback) {
  // this.readUInt8Characteristic(CUSTOM_DATA_UUID, SEGMENT_STATUS_UUID, callback);
  this.readUInt8ArrayCharacteristic(CUSTOM_DATA_UUID, SEGMENT_STATUS_UUID, callback);
};


CustomDataService.prototype.readCallibrationData = function(callback) {
  this.readUInt16LECharacteristic(CUSTOM_DATA_UUID, CALIBRATION_DATA_UUID, callback);
};

/*
4.4.15 Start Calibration (uint8_t, R/W/N)

This characteristic can be written with the value 1 to start the calibration process. 
Before calibration is started, be sure that the Expected Segments and Bank Enable values are set correctly. 
Once calibration is started the client can monitor the Start Calibration characteristic 
to determine when calibration is complete. 
The Bluetooth IR Rack Locator will change the value from 1 (Start) to 0 (Idle) when calibration is complete. 
If an error occurs (IR LED strip not connected or low power supply voltage) the Bluetooth IR Rack Locator 
will change the value to 2 (Fail).
*/
CustomDataService.prototype.notifyCalibrationStatus = function(callback) {
  this.onCalibrationStatusChangeBinded = this.onMeasumentChange.bind(this);
  this.notifyCharacteristic(CUSTOM_DATA_UUID, SEGMENT_STATUS_UUID, true, this.onCalibrationStatusChangeBinded, callback);
};
CustomDataService.prototype.unnotifyCalibrationStatus = function(callback) {
  this.notifyCharacteristic(CUSTOM_DATA_UUID, SEGMENT_STATUS_UUID, false, this.onCalibrationStatusChangeBinded, callback);
};
CustomDataService.prototype.onCallibrationStatusChange = function(data) {
  this.convertMeasument(data, function(counter) {
    this.emit('calibrationStatusChange', counter);
  }.bind(this));
};
CustomDataService.prototype.readStartCalibration = function(callback) {
  this.readUInt8Characteristic(CUSTOM_DATA_UUID, START_CALIBRATION_UUID, callback);
};


CustomDataService.prototype.readAdvertisingConfig = function(callback) {
  this.readStringCharacteristic(CUSTOM_DATA_UUID, ADVERTISING_CONFIG_UUID, callback);
};

module.exports = CustomDataService;

