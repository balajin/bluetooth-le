/* Software Patch Over the Air (SPOTA)
This service is used to update the Bluetooth IR Rack Locator firmware. 
Refer to AN-B-003 “DA14580 Software Patching over the Air (SPOTA)” from Dialog Semiconductor for details. 
Dialog Semiconductor implemented this service for the purpose of supporting firmware patching 
(surgical replacement of specific memory contents and function calls) and later expanded the 
service to support full firmware updates (Software Update Over the Air or SUOTA). 
At this time, the Bluetooth IR Rack Locator only allows full firmware updates. The SPOTA service does not 
accept any write requests unless user authentication has been completed as described in 4.4.5. 
*/
var SPOTA_UUID        = 'fef5';

/*
The Bluetooth IR Rack Locator firmware is stored in SPI Flash. The SPI Flash contains two image banks 
(used in ping-pong fashion) to store firmware updates. If the Bluetooth IR Rack Locator is currently 
running firmware loaded from Bank 1 it will store the firmware update to Bank 2 and if the Bluetooth 
IR Rack Locator is currently running firmware loaded from Bank 2 it will store the firmware update to Bank 1. 
In order to begin the firmware update (SUOTA to SPI Flash) the client must write the 32-bit value 0x13000000 
to this characteristic. After the firmware update has been transferred, the Memory Device characteristic is 
used to send a SUOTA End command and then a SUOTA Reboot command. When the Bluetooth IR Rack Locator reboots, 
it will run the most recent valid firmware.
*/
var MEMORY_DEVICE_UUID  = '8082caa841a6402191c656f9b954cc34';  //4.3.1 Memory Device (uint32_t, R/W)

/* 
This characteristic is not necessary for the Bluetooth IR Rack Locator. 
Clients do not need to write to it.
*/
var GPIO_MAP_UUID       = '724249f05ec34b5f880442345af08651';  ///4.3.2 GPIO Map (uint32_t, R/W)

/*
This characteristic provides progress information back to the client. It indicates how many bytes have 
been written to SPI Flash. Note that the 8-bit checksum (the last byte of Patch Data sent to the Bluetooth 
IR Rack Locator) is not actually written to SPI Flash and does not increment this value.
*/
var MEMORY_INFO_UUID    = '6c53db2547a145fea0227c92fb334fd4';  //4.3.3 Memory Info (uint32_t, R)

/*
This characteristic informs the SPOTA service how many bytes of Patch Data will be sent at one time. 
This is considered the block size. The SPOTA service will wait until this many bytes have been received 
before writing them to SPI Flash. The recommended value is 240 (decimal). After setting the Patch Length, 
the client will begin writing 240-byte blocks of data from the firmware image to the Patch Data characteristic 
and waiting for a Notification on the SPOTA Server Status before writing the next block. The final block 
of data will likely be less than 240 bytes. Before writing the final block of data the client must update 
the Patch Length characteristic with the actual size of the final block (for SUOTA it does not need to be 
a 32-bit aligned value).
*/
var PATCH_LENGTH_UUID   = '9d84b9a3000c49d89183855b673fda31';  //4.3.4 Patch Length (uint16_t, R/W)

/*This characteristic accepts data from the client to be written to SPI Flash. The data are written in Patch 
Length-sized blocks and the next block must not be sent until a Notification via the SPOTA Server Status 
characteristic indicates successful status. The current version of documentation from Dialog Semiconductor 
does not correctly describe this characteristic when used for firmware updates. The firmware image data 
are written as bytes in sequential order. There is no expectation that the data are 32-bit words. The client 
must calculate an 8-bit checksum which is the XOR sum of all bytes in the firmware image and append this 
checksum to the end of the firmware image. For maximum throughput it is recommended that the client use 
a “Write Command” (aka write with no response).*/
var PATCH_DATA_UUID     = '457871e8d5164ca1911657d0b17b9cb2';  //4.3.5 Patch Data (uint8_t[20], R/W/WNR)

/*
This characteristic informs the client of the SPOTA server status. For proper pacing (i.e. flow control) 
when the SPOTA server is writing data to SPI Flash, this characteristic must be enabled for Notifications. 
After a block of patch data is written to the SPOTA server, the client waits for a SPOTA Server Status 
Notification indicating the status of the most recent block of patch data that was sent. Only after 
receiving a Server Status Notification with a status value of SPOTAR_CMP_OK can the client begin 
sending the next block of patch data.

Value   Description
0x00  Reserved 
0x01  SPOTAR_SRV_STARTED. Valid memory device has been configured by initiator. While in this mode, system is kept at active mode.
0x02  SPOTAR_CMP_OK. SPOTA process completed successfully.
0x03  SPOTAR_SRV_EXIT. Forced exit of SPOTAR service. See Table 1.
0x04  SPOTAR_CRC_ERR. Patch Data CRC mismatch.
0x05  SPOTAR_PATCH_LEN_ERR. Received patch Length not equal to PATCH_LEN characteristic value.
0x06  SPOTAR_EXT_MEM_ERR. External Memory Error. Writing to external device failed.
0x07  SPOTAR_INT_MEM_ERR. Internal Memory Error. Not enough internal memory space for patch.
0x08  SPOTAR_INVAL_MEM_TYPE. Invalid memory device.
0x09  SPOTAR_APP_ERROR. Application error.
0x0A-0x0F Reserved
0x10  SPOTAR_IMG_STARTED. SPOTA started for downloading image (SUOTA application)
0x11  SPOTAR_INVAL_IMG_BANK. Invalid image bank
0x12  SPOTAR_INVAL_IMG_HDR. Invalid image header
*/
var SERVER_STATUS_UUID  = '5f78df94798c46f5990ab3eb6a065c88';  //4.3.6 SPOTA Server Status (uint8_t, R/N)

function SpotaService() {
}

SpotaService.prototype.readMemoryDevice = function(callback) {
  this.readUInt32BECharacteristic(SPOTA_UUID, MEMORY_DEVICE_UUID, callback);
};
SpotaService.prototype.writeMemoryDevice = function(value, callback) {
  console.log('Writing characteristic(writeMemoryDevice): '+SPOTA_UUID+', '+MEMORY_DEVICE_UUID+' =', value);
  this.writeUInt32LECharacteristic(SPOTA_UUID, MEMORY_DEVICE_UUID, value, callback);
};

SpotaService.prototype.readGpioMap = function(callback) {
  this.readUInt32LECharacteristic(SPOTA_UUID, GPIO_MAP_UUID, callback);
};
SpotaService.prototype.writeGpioMap = function(value, callback) {
  this.writeUInt32LECharacteristic(SPOTA_UUID, GPIO_MAP_UUID, callback);
};
SpotaService.prototype.readMemoryInfo = function(callback) {
  this.readUInt32LECharacteristic(SPOTA_UUID, MEMORY_INFO_UUID, callback);
};

SpotaService.prototype.readPatchLength = function(callback) {
  this.readUInt16LECharacteristic(SPOTA_UUID, PATCH_LENGTH_UUID, callback);
};
SpotaService.prototype.writePatchLength = function(value, callback) {
  console.log('Writing characteristic(writePatchLength): '+SPOTA_UUID+', '+PATCH_LENGTH_UUID+' =', value);
  this.writeUInt16LECharacteristic(SPOTA_UUID, PATCH_LENGTH_UUID, value, callback);
};

SpotaService.prototype.readPatchData = function(callback) {
  this.readUInt8Characteristic(SPOTA_UUID, PATCH_DATA_UUID, callback);
};
SpotaService.prototype.writePatchData = function(value, callback) {
  // console.log('Writing characteristic(patchData): '+SPOTA_UUID+', '+PATCH_DATA_UUID+' =', value);
  this.writeDataCharacteristic(SPOTA_UUID, PATCH_DATA_UUID, value, callback);
};

SpotaService.prototype.readServerStatus = function(callback) {
  this.readUInt8Characteristic(SPOTA_UUID, SERVER_STATUS_UUID, callback);
};
SpotaService.prototype.notifyServerStatus = function(callback) {
  if(!this.onServerStatusChangeBinded) {
    this.onServerStatusChangeBinded = this.onServerStatusChange.bind(this);
  } // we don't want to overwrite it otherwise
  this.subscribeCharacteristic(SPOTA_UUID, SERVER_STATUS_UUID,this.onServerStatusChangeBinded, callback);
};
SpotaService.prototype.unnotifyServerStatus = function(callback) {
  this.unsubscribeCharacteristic(SPOTA_UUID, SERVER_STATUS_UUID, this.onServerStatusChangeBinded, callback);
};
SpotaService.prototype.onServerStatusChange = function(data) {
  console.log("received ServerStatusChange Event", data);
  this.convertServerStatus(data, function(counter) {
    this.emit('ServerStatusChange', counter);
  }.bind(this));
};

SpotaService.prototype.convertServerStatus = function(data, callback) {
  var flags = data.readUInt8(0);

  if (flags && flags === 0x01) {
    // uint16
    callback(data.readUInt16LE(1));
  } else {
    // uint8
    callback((data.readUInt8(0)).toString(16));
    // callback(data.readUInt8(1));
  }
};

module.exports = SpotaService;

