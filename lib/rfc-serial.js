var com = require("serialport");
var portsList = [];
//var SerialPort = serialport.SerialPort;

var serialPort = new com.SerialPort("/dev/cu.Bluetooth-Incoming-Port", {
  baudrate: 9600,
  parser: com.parsers.readline("\n")
}, false);


//List Ports.  This needs to be tweaked so that we are displaying only the devices which is of interest, not all the serial ports
com.list(function (err, ports) {	
  ports.forEach(function(port) {
    portsList.push(port);
  });
  console.log(portsList);
});
// //Send portList event to the client with the payload of all our devices connected to the port.
// var sendPortsList = function(socket) {	
// 	socket.emit('portsList', portsList);
// };

//Open the serial port, for the device selected by the user
// var openPort = function(port,socket) {
// 	console.log("will try to open the port:", port.comName);

// 	var serialPort = new com.SerialPort(port.comName, 
// 		{
// 			baudrate: 19200, //Default is 9600, but setting it to 19200 can be adjusted based on the RFCode HW docs.
// 			parser: com.parsers.readline("\n") //"readlin" style parsing
// 		}
// 		,false); // this is the openImmediately flag [default is true] 

	serialPort.open(function(error){
		var out=''
		if (error) {
			console.log("failed to open port:" + port.comName + ' Error:' + error);
		} else {
			deviceLogin(serialPort);
			serialPort.on('data', function(data) {
			  console.log('data received: ' + data.toString());
			});			
		}
	});

// };

var deviceLogin = function(serialPort) {
	var cmd = "LOGIN,RFCODE\rver\rSN\rLOC\rIRP\rBRT\rPAT\rS\rRIR\rDC\rSEG\r";
	// writeData(serialPort,cmd);
	writeDataAndDrain(serialPort,cmd);
}
var writeData = function(serialPort,cmd) {
	serialPort.write(cmd, function(err, results){
		if (err) {
			writeErrorCallback(err);
		} 
		else {
			writeSuccessCallback(results);
		}
	});
};
// Writes data and waits until it has finish transmitting to the target serial port before calling the callback.
var writeDataAndDrain = function(serialPort,cmd) {
	serialPort.write(cmd, function(err, results){
		if (err) {
			writeErrorCallback(err);
		} 
		else {
			serialPort.drain(writeSuccessCallback(results));
		}
	});
};
var writeErrorCallback = function(err) {
	console.log("write error:" + err);
};
var writeSuccessCallback = function(results) {
	console.log("write results:" + results);
};