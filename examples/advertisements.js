var RfcBleDevice = require('../index');
var manufacturerID = "8602"; //If you need to include more than one manufacturer ID pass it as an array. for Ex  manufacturerID = ["8602", "8603"];

var SimpleDevice = function(device) {
  RfcBleDevice.call(this, device);
};

/* Refer to Advertisement Packet Section for more details on Advertisement data and what comprises manufactuer data..
  manufacturerData = 
    Manufactuer ID = Ox0286
    Device Type = Ox01
    and Config Hash, Err Status, Tx Power ..

  If we need to filter for just RFCode Ble devices, we can user the manufactuer id.  It is in little endian format
  so, convert the hex value to string, take the 1st 4 bytes and have it reversed.. For Ex: interpret 0286 -> 8602
*/  
SimpleDevice.is = function(device) {
  var manufacturerData = (device.advertisement && device.advertisement.manufacturerData) ?  
    device.advertisement.manufacturerData.toString('hex').substring(0,4) === manufacturerID : 
    false;
  return manufacturerData;
};

RfcBleDevice.Util.inherits(SimpleDevice, RfcBleDevice);
RfcBleDevice.Util.mixin(SimpleDevice, RfcBleDevice.DeviceInformationService);
RfcBleDevice.Util.mixin(SimpleDevice, RfcBleDevice.CustomDataService);

function startScanning() {
  console.log("scanning for RFCode devices... If you want to look for all devices, comment out line 17 thru 22");
  SimpleDevice.discoverAll(function(device) {
    console.log('discovered: ' + device);  
    var peripheral = device._peripheral;
    
    console.log('peripheral discovered (' + peripheral.id +
                ' with address <' + peripheral.address +  ', ' + peripheral.addressType + '>,' +
                ' connectable ' + peripheral.connectable + ',' +
                ' RSSI ' + peripheral.rssi + ':');
    console.log('\thello my local name is:');
    console.log('\t\t' + peripheral.advertisement.localName);
    
    if (peripheral.advertisement.manufacturerData) {
      console.log('\tmy manufacturer data:');
      console.log('\t\t' + JSON.stringify(peripheral.advertisement.manufacturerData.toString('hex')));
    }

    console.log();
  });
};

startScanning();
