var async = require('async');

var LocatorDevice = require('../index');

var MyAwesomeDevice = function(peripheral) {
  LocatorDevice.call(this, peripheral);
};

LocatorDevice.Util.inherits(MyAwesomeDevice, LocatorDevice);

MyAwesomeDevice.discover(function(peripheralDevice) {
  console.log('found ' + peripheralDevice.id);

  peripheralDevice.on('disconnect', function() {
    console.log('disconnected!');
    process.exit(0);
  });

  async.series([
      function(callback) {
        console.log('connect');
        peripheralDevice.connect(callback);
      },
      function(callback) {
        console.log('discoverServicesAndCharacteristics');
        peripheralDevice.discoverServicesAndCharacteristics(callback);
      },
      function(callback) {
        console.log('readDeviceName');
        peripheralDevice.readDeviceName(function(error, deviceName) {
          console.log('\tdevice name = ' + deviceName);
          callback();
        });
      },
      function(callback) {
        console.log('disconnect');
        peripheralDevice.disconnect(callback);
      }
    ]
  );
});
