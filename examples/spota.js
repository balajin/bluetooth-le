var RfcBleDevice = require('../index');
var fs = require('fs');
var idOrLocalName = process.argv[2];
var imageFileName = process.argv[3];

if(!idOrLocalName || !imageFileName) {
	console.log("node spota.js <ID or local name> <image filename>");
	process.exit(1);
}

var SimpleDevice = function(device) {
	RfcBleDevice.call(this, device);
};

SimpleDevice.is = function(device) {
	var localName = device.advertisement.localName;
	return (device.id === idOrLocalName || localName === idOrLocalName);
};

RfcBleDevice.Util.inherits(SimpleDevice, RfcBleDevice);
RfcBleDevice.Util.mixin(SimpleDevice, RfcBleDevice.DeviceInformationService);
RfcBleDevice.Util.mixin(SimpleDevice, RfcBleDevice.SpotaService);

function updateFirmware(device, imageFile, callback) {
	// NOTICE: by the time this is called, device should be already
	// connected and authorized
	// or else we'll fail
	
	var finalizers = [];
	var terminated = false;
	function finish(error, value) {
		if(terminated) {
			console.warning('Subsequent finish call');
			if(error) {
				console.error(error);
			}
			return;
		}
		terminated = true;
		// run all finalizers
		finalizers.forEach(fin => fin(error));

		if(error) {
			// log error to console
			console.error(error);
		}

		// call original callback
		callback(error, value);
	}
	function abortError(message, error) {
		console.error(message);
		finish(error || new Error(message));
	}
	/*
	 * Factory which produces a proper "error-first" callback.
	 * First argument of factory is a message to append to the error if one happens.
	 * Second argument is a "success-case" callback.
	 */
	function safe(errmsg, success) {
		return (error, result) => {
			if(error) {
				abortError(errmsg, error);
			} else {
				if(typeof success === 'function') {
					success(result);
				}
			}
		};
	}

	var firmware;
	var firmwareSize;
	var blockSize = 240; // this is a recommended default

	function loadFirmware() {
		// First of all, let's read the firmware file
		firmware = fs.readFileSync(imageFile);
		// Calculate checksum: 8-bit xor sum of the whole firmware image
		var checksum = 0;
		firmware.forEach(function(b) {
			checksum ^= b;
		});
		// Append checksum to the end of firmware
		firmware = Buffer.concat([firmware, Buffer([checksum])]);

		// Pass to stage 1
		initializeTransfer();
	}
	
	function initializeTransfer() {
		// Step 1. Define memory type and bank selection.
		// For our device, we should just set a fixed value
		device.writeMemoryDevice(0x13000000, safe('initialization failed', setBasicBlockSize));
	}

	function setBasicBlockSize() {
		// Step 2. Devine GPIO mapping - not needed for our device.
		// Step 3. Define block size
		firmwareSize = firmware.length;
		if(firmwareSize >= blockSize) {
			device.writePatchLength(blockSize, safe('initial writePatchLength failed', prepareStatusCallback));
		} else {
			// Special case: if the whole patch is smaller than block
			// then we don't want to set blocksize now
			// and instead jump directly to stage 4
			prepareStatusCallback();
		}
	}

	function prepareStatusCallback() {
		// Do send blocks one by one
		// but first subscribe to notificaitons about server status
		device.on('ServerStatusChange', statusCallback);
		device.notifyServerStatus(safe('notifyServerStatus failed', sendBlock.bind(null, 0)));
		// don't forget to unsubscribe when done!
		finalizers.push(() => {
			// Unsubscribe from status notifications
			device.unnotifyServerStatus();
			device.removeListener('ServerStatusChange', statusCallback);
		});
	}

	var onNextOkay;  // what should we do when we get next OK status
	function statusCallback(value) {
		console.log('SPOTA status: ' + value);
		if(value != 0x01 /* SPOTAR_SRV_STARTED */ && value != 0x02 /* SPOTAR_CMP_OK */) {
			return abortError('Got error status code from SPOTA: ' + value);
		}
		if(!onNextOkay) {
			return abortError('Got OK status, but nothing to do!');
		}
		var next = onNextOkay;
		onNextOkay = null;
		next();
	}

	function waitForOkay() {
		// return; // - with actual device, nothing to do here?
		rl.question('Which server status shall we use?', val => {
			statusCallback(parseInt(val));
		});
	}

	function sendBlock(start) {
		var block = firmware.slice(start, start+blockSize);
		var currBlockSize = block.length;

		var sendChunks = function() {
			// same approach as with sendBlock itself
			var sendChunk = function(cstart) {
				var chunk = block.slice(cstart, cstart+20);
				var next;
				if(chunk.length == 20) {
					next = sendChunk.bind(null, cstart+20);
				} else {
					// else we shall wait for OK message
					next = waitForOkay;
				}
				if(chunk.length) {
					device.writePatchData(chunk, safe('Failed to send chunk', next));
				} else {
					// this chunk is empty - no need to send
					next();
				}
			};
			sendChunk(0);
		};

		if(currBlockSize === 0) {
			// this is the last block and it is empty; pass to next stage
			return checkSize();
		}

		if(currBlockSize == blockSize) {
			// Most likely this is a regular block; schedule next one sending.
			// Even if this is actually the last one which accidentally has even size,
			// we'll detect that on next launch.
			onNextOkay = sendBlock.bind(null, start + blockSize);
			// now we want to send our chunks
			return sendChunks();
		} else {
			// This is a final block.
			// Schedule next stage
			onNextOkay = checkSize;
			// and notify device about decreased block size
			// before starting sending chunks
			device.writePatchLength(currBlockSize, safe('Failed to send final patchLength', sendChunks));
		}
	}

	function checkSize() {
		// Check that total number of bytes is correct
		device.readMemoryInfo(safe('Failed to read memory info', bytesWritten => {
			// Bytes written should be 1 lesser than fw length,
			// because checksum byte is not actually written to the flash.
			if(bytesWritten != firmware.length - 1) {
				abortError('Fatal: number of bytes written is wrong! Expected ' + (firmware.length - 1) + ', got ' + bytesWritten);
			} else {
				// Everything is good - so pass to the last stage
				finalizeTransfer();
			}
		}));
	}

	function finalizeTransfer() {
		// everything is good, so let's indicate end of transfer
		// but first schedule next action
		onNextOkay = rebootDevice;
		device.writeMemoryDevice(0xFE000000, safe('end of transfer failed'));
		waitForOkay();
	}

	function rebootDevice() {
		// now it is safe to reboot device
		device.writeMemoryDevice(0xFD000000, safe('failed to reboot device', () => {
			// We are done!
			finish(null, 'Success');
		}));
	}

	console.log('updating fw on device: ' + device);

	device.on('disconnect', () => {
		abortError('Connection lost!');
		// It will auto mark us as terminated, so that any subsequent callbacks will not call "master" callback anymore
	});

	// initiate the sequence of steps
	loadFirmware();
}

var device = new SimpleDevice({
	id: 1, uuid: '1234', address: 'addr', addressType: 'bt',
});
device.readDataCharacteristic = function(svc, ch, cb) {
	console.log('Reading characteristic: '+svc+', '+ch);
	var result = new Buffer([0x00, 0x00, 0x04, 0x4d]);
	cb(null, result);
}
device.writeDataCharacteristic = function(svc, ch, data, cb) {
	console.log('Writing characteristic: '+svc+', '+ch+' =', data);
	cb(null);
}
device.notifyCharacteristic = function(svc, ch, doNotify, listener, cb) {
	// TODO save listener?
	cb && cb(null);
}
var readline = require('readline');
var rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout,
});

// Obtain device in any way, e.g. as described in examples/deviceWithRFCAuth.js
// Then do this:
updateFirmware(device, imageFileName, function(error) {
	rl.close();
	if(error) {
		console.log('Failed to update firmware');
		console.log(error);
		return;
	}
	console.log('Updated successfully!');
});
