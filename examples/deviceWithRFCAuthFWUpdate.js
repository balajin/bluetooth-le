/*
my test device with pwd enabled (red lable on back)
node deviceWithRFCAuth.js 90b6860c0898

Ryan's Fixture
node deviceWithRFCAuth.js 90b686028d05
*/

var RfcBleDevice = require('../index');
var idOrLocalName = process.argv[2];
var deriveAuthKeySha256 = require('../lib/auth_key_sha');
var updateFirmware = require('../lib/firmware_update');

if (!idOrLocalName) {
  console.log("node simple-device.js [ID or local name]");
  process.exit(1);
}

var SimpleDevice = function(device) {
  RfcBleDevice.call(this, device);
};

SimpleDevice.is = function(device) {
  var localName = device.advertisement.localName;
  return (device.id === idOrLocalName || localName === idOrLocalName);
};

RfcBleDevice.Util.inherits(SimpleDevice, RfcBleDevice);
RfcBleDevice.Util.mixin(SimpleDevice, RfcBleDevice.DeviceInformationService);
RfcBleDevice.Util.mixin(SimpleDevice, RfcBleDevice.CustomDataService);
RfcBleDevice.Util.mixin(SimpleDevice, RfcBleDevice.SpotaService);

function onDiscover(device) {
  console.log('discovered: ' + device);

  device.on('disconnect', function() {
    console.log('disconnected!');
    process.exit(0);
  });

  device.connectAndSetUp(function(callback) {
    console.log('connectAndSetUp');
    var alertLevel = '2';

    device.readAuthToken(function(error, data) {
     if (error) {
       console.log('\treadAuthToken unsuccessful' + error);
     } else {
       console.log('\treadAuthToken = ' + data); 
       const buf1 = deriveAuthKeySha256(data, 'RFCode');
       console.log('\tGenerated Auth Key(buffer) = ', buf1);
       console.log('\tGenerated Auth Key(hex) = ', buf1.toString('hex'));

       device.writeAuthKey(deriveAuthKeySha256(data, 'RFCode'), function(error) {
        if (error) {
          console.log("in writeAuthKey callback:" + error);
        } else {
          console.log("in writeAuthKey callback2:" + error);     
          // device.writeAlertLevel(alertLevel, function(error) {
          //   if (error) {
          //     console.log('\twriteAlertLevel to ' + alertLevel + ' unsuccessful' + error);
          //   } else {
              console.log('\twriteAlertLevel to ' + alertLevel + ' Successful LED should blink faster now');
              device.readModelNumber(function(error, data) {
                console.log('\treadModelNumber = ' + data);
              });
              device.readFirmwareRevision(function(error, data) {
                console.log('\treadFirmwareRevision = ' + data);
              });
              device.readSoftwareRevision(function(error, data) {
                console.log('\treadSoftwareRevision = ' + data);
              });

              // setTimeout(() => {
              //   device.disconnect();
              // }
              // ,5000);            
              var readline = require('readline');
              var rl = readline.createInterface({
                input: process.stdin,
                output: process.stdout,
              }); 
              updateFirmware(device, 'yellowstone993.img', function(error) {
                // rl.close();
                if(error) {
                  console.log('Failed to update firmware');
                  console.log(error);
                  return;
                }
                console.log('Updated successfully!');
              });
          //   }
          // }); //End of writeAutyKey

        } //End of else    
      });
     }
   });        
  });
};

SimpleDevice.discover(onDiscover);