var RfcBleDevice = require('../index');
var idOrLocalName = process.argv[2];

if (!idOrLocalName) {
  console.log("node simple-device.js [ID or local name]");
  process.exit(1);
}

var SimpleDevice = function(device) {
  RfcBleDevice.call(this, device);
};

SimpleDevice.is = function(device) {
  var localName = device.advertisement.localName;
  return (device.id === idOrLocalName || localName === idOrLocalName);
};

RfcBleDevice.Util.inherits(SimpleDevice, RfcBleDevice);
RfcBleDevice.Util.mixin(SimpleDevice, RfcBleDevice.DeviceInformationService);
RfcBleDevice.Util.mixin(SimpleDevice, RfcBleDevice.CustomDataService);

function onDiscover(device) {
  console.log('discovered: ' + device);

  device.on('disconnect', function() {
    console.log('disconnected!');
    process.exit(0);
  });

  device.connectAndSetUp(function(callback) {
    console.log('connectAndSetUp');

    device.readModelNumber(function(error, data) {
      console.log('\treadModelNumber = ' + data);
    });
    device.readFirmwareRevision(function(error, data) {
      console.log('\treadFirmwareRevision = ' + data);
    });
    device.readSoftwareRevision(function(error, data) {
      console.log('\treadSoftwareRevision = ' + data);
    });

    var alertLevel = '1';

        // device.writeAlertLevel(alertLevel, function(error) {
        //     if (error) {
        //         console.log('\twriteAlertLevel to ' + alertLevel + 'unsuccessful' + error);
        //     }

        //     console.log('\twriteAlertLevel to ' + alertLevel + 'Successful LED should blink faster now');
        // });
  });
};

SimpleDevice.discover(onDiscover);
// SimpleDevice.discoverAll(onDiscover);
// SimpleDevice.discoverById('3d7bd14925ad4f69b73a98cbc998c4db', onDiscover);
// SimpleDevice.discoverByAddress('90:59:af:0a:ab:34', onDiscover);

//Tryouts
// Stream write function
/*
BLESerial.prototype._write = function (chunk, encoding, callback) {
  // Do we have a characteristic?
  if (this._characteristic) {
    // Then write our data to it, in 20 byte chunks
    for (var i = 0; i < chunk.length; i += 20) {
      this._characteristic.write(chunk.slice(i, i + 20), true);
    }
    // Everything is fine
    callback();
  } else {
    // There's a problem
    callback(new Error("No connection"));
  }
}
*/