var RfcBleDevice = require('./lib/rfc-ble');

RfcBleDevice.Util = require('./lib/util');
RfcBleDevice.DeviceInformationService = require('./lib/device-information-service');
RfcBleDevice.CustomDataService = require('./lib/custom-data-service');
RfcBleDevice.SpotaService = require('./lib/spota-service');

module.exports = RfcBleDevice;
