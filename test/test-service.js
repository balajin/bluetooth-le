var should = require('should');
var sinon = require('sinon');

var Service = require('../lib/rfc-ble');

describe('service', function() {
  var mockNoble = null;
  var mockPeripheralId = 'mock-peripheral-id';
  var mockUuid = 'mock-uuid';

  var mockPeripheral = {
    'id' : mockPeripheralId,
    'uuid' : mockUuid,
    'address' : 'mock-peripheral-address',
    'addressType' : 'mock-address-type',
    'on' : sinon.spy(),
    'discoverServicesAndCharacteristics' : sinon.spy(),
    'discoverAllServicesAndCharacteristics' : sinon.spy()
  }
  var service = null;

  beforeEach(function() {
    mockNoble = {
      connect: sinon.spy(),
      disconnect: sinon.spy(),
      updateRssi: sinon.spy(),
      discoverServices: sinon.spy(),
      readHandle: sinon.spy(),
      // writeHandle: sinon.spy()
      discoverIncludedServices: sinon.spy(),
      discoverCharacteristics: sinon.spy(),
      discoverServicesAndCharacteristics: sinon.spy()
    };
    service = new Service(mockPeripheral);
  });

  afterEach(function() {
    service = null;
  });

  it('should have a uuid', function() {
    console.log("service.uuid:" + service.uuid);
    service.uuid.should.equal(mockUuid);
  });

  it('should be uuid, name, type, includedServiceUuids', function() {
    console.log("service.toString" + service.toString());
    service.toString().should.equal('{"uuid":"mock-uuid"}');
  });
});