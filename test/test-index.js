var should = require('should');

describe('index', function() {
    it('should export module Util.inherits', function() {
        var index = require('../index');
        (index.Util.inherits).should.be.ok;
    });
});

describe('index.CustomDataService', function() {
    it('should export module CustomDataService', function() {
        var index = require('../index');
        (index.CustomDataService).should.be.ok;
    });
});

describe('index.DeviceInformationService', function() {
    it('should export module DeviceInformationService', function() {
        var index = require('../index');
        (index.DeviceInformationService).should.be.ok;
    });
});

describe('index.SpotaService', function() {
    it('should export module SpotaService', function() {
        var index = require('../index');
        (index.SpotaService).should.be.ok;
    });
});
